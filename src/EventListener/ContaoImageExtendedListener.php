<?php

// src/EventListener/ParseTemplateListener.php
namespace designerei\ContaoImageExtendedBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;

/**
 * @Hook("parseTemplate")
 */
class ContaoImageExtendedListener
{
    public function __invoke(Template $template): void
    {
        if ('image' === $template->getName()) {

            $picture      = $template->picture;
            $imgFluid     = $template->imgFluid;
            $imgObjectFit = $template->imgObjectFit;
            $floatClass   = $template->floatClass;

            // default settings
            $floatClass       .= ' ' . 'h-full';

            // image fluid
            if(false == $imgFluid) {
                $picture['class'] .= ' ' . 'w-full';
            }

            // image object-fit
            if($imgObjectFit) {
                $picture['class'] .= ' ' . 'object-cover h-full';
            }

            $template->picture    = $picture;
            $template->floatClass = $floatClass;
        }
    }
}
